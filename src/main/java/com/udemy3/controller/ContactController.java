package com.udemy3.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.udemy3.constant.ViewConstant;
import com.udemy3.model.ContactModel;
import com.udemy3.service.ContactService;

@Controller
@RequestMapping("/contacts")
public class ContactController {
	
	private static final Log log = LogFactory.getLog(ContactController.class);
	@Autowired
	@Qualifier("contactServiceImpl")
	private ContactService contactService;
	

	@GetMapping("/cancel")
	public String cancel(){
		return "redirect:/contacts/showcontacts";
	}
	
	@PreAuthorize("hasRole('ROLE_USER2')")
	@GetMapping("/contactform")
	public String redirectContactForm(@RequestParam(name="id", required=false) Integer id,
			Model model){
		ContactModel contact = id==null || id==0 ? null :  contactService.findContactByIdModel(id);
		
		model.addAttribute("contactModel", contact == null ? new ContactModel() : contact);
		return ViewConstant.CONTACT_FORM;
	}
	
	@PostMapping("/addcontact")
	public String addContact(@ModelAttribute(name="contactModel") ContactModel contactModel, Model model){
		log.info("METHOD: addContact() -- PARAMS: "+contactModel);
		
		if(null != contactService.addContact(contactModel)){
			model.addAttribute("result", 1);
		}else{
			model.addAttribute("result", 0);
		}
		
		
		return "redirect:/contacts/showcontacts";
	}
	
	@GetMapping("/showcontacts")
	public ModelAndView showContacts(){
		ModelAndView mav = new ModelAndView(ViewConstant.CONTACTS);
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		mav.addObject("username", user.getUsername());
		mav.addObject("contacts", contactService.listAllContacts());
		
		return mav;
	}
	
	@GetMapping("/removecontact")
	public ModelAndView removeContact(@RequestParam(name="id", required=true) Integer id){
		contactService.removeContact(id);
		return showContacts();
	}
}
