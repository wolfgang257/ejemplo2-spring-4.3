package com.udemy3.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.udemy3.constant.ViewConstant;

@Controller
public class LoginController {

	private static final Log LOG = LogFactory.getLog(LoginController.class);

	/*@GetMapping("/")
	public String redirectToLogin() {
		LOG.info("To login redirect");
		return "redirect:/login";
	}*/

	@GetMapping("/login")
	public String showLoginForm(Model model, @RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
		LOG.info("In login with: " + error + "; " + logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		//model.addAttribute("userCredential", new UserCredential());
		return ViewConstant.LOGIN;
	}

	/*
	 * @PostMapping("/logincheck") public String
	 * loginCheck(@ModelAttribute(name="userCredential") UserCredential
	 * userCredential){ if(userCredential.getUsername().equals("user") &&
	 * userCredential.getPassword().equals("user")){ LOG.info("To loginCheck() params: "+userCredential.toString());
	 * return "redirect:/contacts/showcontacts"; }
	 * LOG.info("To login error copio"); return "redirect:/login?error"; }
	 */

	@GetMapping({"/loginsuccess", "/"})
	public String loginCheck() {

		LOG.info("To loginCheck()");
		return "redirect:/contacts/showcontacts";

	}
}
