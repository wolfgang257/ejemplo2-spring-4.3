package com.udemy3.controller;

import com.udemy3.entity.Contact;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.udemy3.model.ContactModel;
import com.udemy3.service.ContactService;
import com.udemy3.service.impl.GeneralJPAService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest")
public class RestController {

    @Autowired
    @Qualifier("contactServiceImpl")
    private ContactService contactService;

    @Autowired
    @Qualifier("generalJPAService")
    private GeneralJPAService generalJPAService;

    @GetMapping("/checkrest")
    public ResponseEntity<ContactModel> checkRest() {
        ContactModel cm = new ContactModel(2, "Julian", "Os", "222", "Mede");
        return new ResponseEntity<ContactModel>(cm, HttpStatus.OK);
    }
    
    @GetMapping("/listarcontactos")
    public ResponseEntity<List<Contact>> listarcontactos() {
        
        return new ResponseEntity<List<Contact>>(generalJPAService.findAllPersonas2(), HttpStatus.OK);
    }
}
