package com.udemy3.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.udemy3.component.ContactConverter;
import com.udemy3.entity.Contact;
import com.udemy3.model.ContactModel;
import com.udemy3.repository.ContactRepository;
import com.udemy3.service.ContactService;

@Service("contactServiceImpl")
public class ContactServiceImpl implements ContactService{

	@Autowired
	@Qualifier("contactRepository")
	private ContactRepository contactRepository;
	
	@Autowired
	@Qualifier("contactConverter")
	private ContactConverter contactConverter;
	
	@Override
	public ContactModel addContact(ContactModel contactModel) {
		
		Contact contact =  contactRepository.save(contactConverter.convertContactModel2Contact(contactModel));
		return contactConverter.convertContact2ContactModel(contact);
	}

	@Override
	public List<ContactModel> listAllContacts() {
		return contactConverter.convertContact2ContactModel(contactRepository.findAll());
	}

	@Override
	public Contact findContactById(Integer id) {
		return contactRepository.findById(id);
	}
	
	@Override
	public ContactModel findContactByIdModel(Integer id){
		Contact contact =  findContactById(id);
		if(contact==null){
			return null;
		}else{
			return contactConverter.convertContact2ContactModel(contact);
		}
	}

	@Override
	public void removeContact(Integer id) {
		Contact contact = contactRepository.findById(id);
		if(contact!=null){
			contactRepository.delete(contact);
		}
		
	}

}
