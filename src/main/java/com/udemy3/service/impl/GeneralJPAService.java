/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.udemy3.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import com.udemy3.entity.Contact;
import com.udemy3.entity.QContact;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Pruebas con QueryDSL y otros
 *
 * @author josorio
 */
@Repository("generalJPAService")
public class GeneralJPAService {

    QContact qContact = QContact.contact;

    @PersistenceContext
    private EntityManager em;

//    /**
//     * Necesita que se configure el EntityManagerFactory para hacer un unwrap
//     */
//    @Autowired
//    private SessionFactory sessionFactory;
    
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    /**
     *
     * @param exist
     * @return
     */
    public Contact find(boolean exist) {

        JPAQuery<Contact> query = new JPAQuery<Contact>(em);

        BooleanBuilder predicateBuilder = new BooleanBuilder(qContact.firstname.endsWith("OP"));
        if (exist) {
            Predicate predicate2 = qContact.id.eq(23);
            predicateBuilder.and(predicate2);
        } else {
            predicateBuilder.or(qContact.firstname.endsWith("OP"));
        }

        return query.select(qContact).from(qContact).where(predicateBuilder).fetchOne();

        //List<Course> courses = query.select(qCourse).from(qCourse).where(qCourse.hours.between(13, 50)).fetch();
    }

    public Contact findPersonaById(long id) {
        return em.find(Contact.class, id);
    }

//    @Override
    public List<Contact> findAllPersonas() {
        String jpql = "SELECT p FROM Contact p";
        Query query = em.createQuery(jpql);
        List<Contact> personas = query.getResultList();
        System.out.println("personas:" + personas);
        return personas;
    }

//    @Override
    public long contadorPersonas() {
        String consulta = "select count(p) from Contact p";
        Query q = em.createQuery(consulta);
        return (long) q.getSingleResult();
    }

//    @Override
    public Contact getPersonaByEmail(Contact persona) {
        String cadena = "%" + persona.getFirstname() + "%"; //se usa en el like como caracteres especiales
        String consulta = "from Persona p where upper(p.firstname) like upper(:param1)";
        Query q = em.createQuery(consulta);
        q.setParameter("param1", cadena);
        return (Contact) q.getSingleResult();
    }

//    @Override
    public void insertPersona(Contact persona) {
        // Insertamos nuevo objeto
        em.persist(persona);
    }

//    @Override
    public void updatePersona(Contact persona) {
        // Actualizamos al objeto 
        em.merge(persona);
    }

//    @Override
    public void deletePersona(Contact persona) {
        em.remove(em.merge(persona));
    }

    private Session currentSession() {
        Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession();// getCurrentSession();
        return session;
    }

    @SuppressWarnings("unchecked")
    public List<Contact> findAllPersonas2() {
        return currentSession().createQuery("from Contact").list();
    }
}
