package com.udemy3.service;

import java.util.List;

import com.udemy3.entity.Contact;
import com.udemy3.model.ContactModel;

public interface ContactService {

	/**
	 * Guardar modelo usando entidad
	 * @param contactModel
	 * @return
	 */
	public ContactModel addContact(ContactModel contactModel);
	
	/**
	 * Lista todos los contactos
	 * @return
	 */
	public List<ContactModel> listAllContacts();
	
	/**
	 * Buscar contacto
	 * @param id
	 * @return
	 */
	public Contact findContactById(Integer id);
	
	/**
	 * Buscar contacto
	 * @param id
	 * @return
	 */
	public ContactModel findContactByIdModel(Integer id);
	
	/**
	 * Eliminar contacto
	 * @param id
	 */
	public void removeContact(Integer id);
}
