package com.udemy3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Backendninja3Application {

	public static void main(String[] args) {
		SpringApplication.run(Backendninja3Application.class, args);
	}
}
