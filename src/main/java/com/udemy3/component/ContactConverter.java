package com.udemy3.component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.udemy3.entity.Contact;
import com.udemy3.model.ContactModel;

// TODO: Auto-generated Javadoc
/**
 * The Class ContactConverter.
 *
 * @author josorio
 */
@Component("contactConverter")
public class ContactConverter {

	/**
	 * Convert contact model 2 contact.
	 *
	 * @param model the model
	 * @return the contact
	 */
	public Contact convertContactModel2Contact(ContactModel model){
		Contact entidad = new Contact();
		entidad.setCity(model.getCity());
		entidad.setFirstname(model.getFirstname());
		entidad.setId(model.getId());
		entidad.setLastname(model.getLastname());
		entidad.setTelephone(model.getTelephone());
		return entidad;
	}
	
	/**
	 * Convert contact 2 contact model.
	 *
	 * @param entity the entity
	 * @return the contact model
	 */
	public ContactModel convertContact2ContactModel(Contact entity){
		ContactModel model = new ContactModel();
		model.setCity(entity.getCity());
		model.setFirstname(entity.getFirstname());
		model.setId(entity.getId());
		model.setLastname(entity.getLastname());
		model.setTelephone(entity.getTelephone());
		return model;
	}
	
	/**
	 * Convert contact 2 contact model.
	 *
	 * @param list the list
	 * @return the list
	 */
	public List<ContactModel> convertContact2ContactModel(List<Contact> list){
		List<ContactModel> retorno = new ArrayList<>(); 
		for (Contact contact : list) {
			retorno.add(convertContact2ContactModel(contact));
		}
		return retorno;
	}
}
